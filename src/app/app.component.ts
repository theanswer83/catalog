import { Component } from '@angular/core';

@Component({
  selector: 'md-root',
  template: `
    <md-navbar></md-navbar>
    <div class="container mt-3">
        <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'catalog';
}
