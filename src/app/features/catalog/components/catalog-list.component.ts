import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Device} from '../model/device';

@Component({
  selector: 'md-catalog-list',
  template: `
    <div class="list-group-item"
         *ngFor="let device of devices"
         (click)="setActive.emit(device)"
         [ngClass]="{active: device.id === active?.id}"
    >

      <md-os-icon [os]="device.os"></md-os-icon>

      {{device.label}}
      <div class="pull-right">
        <md-price [price]="device.price"></md-price>
        <i class="fa fa-trash" (click)="deleteHandler($event, device)"></i>
      </div>
    </div>
  `,
  styles: []
})
export class CatalogListComponent {
  @Input() devices: Device[];
  @Input() active: Device;
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>();

  deleteHandler(event: MouseEvent, device: Device): void {
    event.stopPropagation();
    this.delete.emit(device);
  }
}
