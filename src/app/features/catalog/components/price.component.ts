import {Component, Input} from '@angular/core';

@Component({
  selector: 'md-price',
  template: `
    <span>{{price | number: '1.2-2'}}</span>
  `,
  styles: [
  ]
})
export class PriceComponent {
  @Input() price: number;
}
