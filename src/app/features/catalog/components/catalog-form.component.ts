import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from '@angular/core';
import {Device} from '../model/device';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'md-catalog-form',
  template: `
    <form #f="ngForm" (submit)="saveHandler()">
      <input
        type="text"
        [ngModel]="active?.label"
        name="label"
        class="form-control"
        placeholder="Model"
        required>
      <input
        type="number"
        [ngModel]="active?.price"
        name="price"
        class="form-control"
        placeholder="Price"
      >
      <div class="btn-group">
        <button
          type="submit"
          class="btn btn-info"
        >
          {{active?.id ? 'EDIT' : 'ADD'}}
        </button>
        <button
          type="button"
          class="btn btn-warning"
          (click)="resetHandler()"
          *ngIf="active"
        >
          RESET
        </button>

      </div>
    </form>
  `,
  styles: []
})
export class CatalogFormComponent implements OnChanges {
  @Input() active: Device;
  @Output() save: EventEmitter<Device> = new EventEmitter();
  @Output() reset: EventEmitter<any> = new EventEmitter();
  @ViewChild('f') form: NgForm;

  ngOnChanges(changes: SimpleChanges): void {
    const active: Device = changes.active.currentValue;
    if (!active?.id) {
      this.form?.reset();
    }
  }

  saveHandler(): void {
    this.save.emit(this.form.value);
  }

  resetHandler(): void {
    this.reset.emit();
    this.form.reset();
  }
}
