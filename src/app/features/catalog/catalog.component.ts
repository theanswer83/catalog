import {Component} from '@angular/core';
import {CatalogService} from './services/catalog.service';
import {CatalogStore} from './services/catalog.store';

@Component({
  selector: 'md-catalog',
  template: `
    <md-card title="FORM">
      <md-catalog-form
       [active]="store.active"
       (save)="actions.save($event)"
       (reset)="actions.reset()"
      ></md-catalog-form>
    </md-card>

    <md-card title="LIST">
      <md-catalog-list
          [devices]="store.devices"
          [active]="store.active"
          (setActive)="actions.setActive($event)"
          (delete)="actions.deleteHandler($event)"
      ></md-catalog-list>
    </md-card>
  `,
  styles: []
})
export class CatalogComponent {

  constructor(
    public actions: CatalogService,
    public store: CatalogStore
  ) {
  actions.getAll();
  }

}
