import {Injectable} from '@angular/core';
import {Device} from '../model/device';

@Injectable({
  providedIn: 'root'
})
export class CatalogStore {
  devices: Device[];
  active: Device;

  setActive(device: Device): void {
    this.active = device;
  }

  load(devices: Device[]): void {
    this.devices = devices;
  }

  add(device: Device): void {
    this.devices.push(device);
    this.active = {};
  }

  edit(device: Device): void {
    const index = this.devices.findIndex(item => item.id === this.active.id);
    this.devices[index] = device;
  }

  delete(id: number): void {
    const index = this.devices.findIndex(item => item.id === id);
    this.devices.splice(index, 1);
  }

  reset(): void {
    this.active = {};
  }
}
