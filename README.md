# Booking

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## Run client

Run `npm start`. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Run server

Run `npm run server` for load db.json.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
